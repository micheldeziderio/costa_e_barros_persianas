	/*		var $doc = $('html, body');
			$('.scrollTop').click(function() {
				$doc.animate({
					scrollTop: $( $.attr(this, 'href') ).offset().top
				}, 500);
				return false;
			});
	*/
			
			// document.querySelector(".dg-login").placeholder = 'Digite o seu login <img src="./_assets/images/layout/icon-login.png">';
			// document.querySelector(".dg-password").placeholder = 'Digite a sua senha';
			
			$('ul.nav li.dropdown').hover(function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
			}, function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
			});

			$('.navbar-toggle').click(function(){
				$('.dg-menu-mobile').show(600);
			});

			$('.navbar-toggle').click(function(){
				$('.dg-menu-mobile').show(600);
			});

			$('.fecha-menu a').click(function(){
				$('.dg-menu-mobile').hide(600);
			});

			$('.drop-shadow').click(function(){
				$('.dg-menu-mobile').hide(600);
			});


			$('.dg-icones-b a').click(function(){
				$('#boxInfoOne').show(1000);
			});

			$('.dg-close-info a').click(function(){
				$('#boxInfoOne').hide(1000);
			});


			$(document).on('ready', function() {
				$(".dg-slider").slick({
					dots: true,
					//autoplay:true,
					infinite: true
				});				
			});    

			$(document).on('ready', function() {
				$(".dg-products").slick({
					dots: true,
					//autoplay:true,
					infinite: true
				});				
			});    

			$(document).on('ready', function() {
				$(".dg-slider-services").slick({
					dots: true,
					slidesToShow: 2,
					infinite: true,
					responsive: [
						{
						breakpoint: 950,
						settings: {
							arrows: true,
							centerMode: true,
							slidesToShow: 1
						}
						}

					,
					
						{
						breakpoint: 768,
						settings: {
							arrows: true,
							centerMode: false,
							slidesToShow: 1
						}
						}

				]
				
				});				
			});    

			$('#dg-comocomprar-bullets .col-md-4 a[data-slide]').click(function(e) {
				$('#dg-comocomprar-bullets .col-md-4 a').removeClass('dg-ativo');
    			$(this).addClass('dg-ativo');

				e.preventDefault();
				var slideno = $(this).data('slide');
				$('#dg-slider-services').slick('slickGoTo', slideno - 1);
			});

			$('.jsBounceInRight').ready().addClass('bounceInRight');
			$('.jsBounceInLeft').ready().addClass('bounceInLeft');
			

			
			/*      EFEITO IMAGENS NEO-SPLIT      */

			$(".dg-box-neosplit3 .col-md-4:nth-child(1) img").hover(function(){
				$('.dg-box-neosplit3 .col-md-4:nth-child(1)').addClass("pa-img-active");
				$('.dg-box-neosplit3 .col-md-4:nth-child(2)').removeClass("pa-img-active");
				$('.dg-box-neosplit3 .col-md-4:nth-child(3)').removeClass("pa-img-active");
			});

			$(".dg-box-neosplit3 .col-md-4:nth-child(2) img").hover(function(){
				$('.dg-box-neosplit3 .col-md-4:nth-child(2)').addClass("pa-img-active");
				$('.dg-box-neosplit3 .col-md-4:nth-child(1)').removeClass("pa-img-active");
				$('.dg-box-neosplit3 .col-md-4:nth-child(3)').removeClass("pa-img-active");
			});

			$(".dg-box-neosplit3 .col-md-4:nth-child(3) img").hover(function(){
				$('.dg-box-neosplit3 .col-md-4:nth-child(3)').addClass("pa-img-active");
				$('.dg-box-neosplit3 .col-md-4:nth-child(1)').removeClass("pa-img-active");
				$('.dg-box-neosplit3 .col-md-4:nth-child(2)').removeClass("pa-img-active");
			});

			
			/*      TOGGLE CLASS LISTA      */

			$(function(){
				$('.jsToggleValue').click(function(event) {
					$('.jsToggleValue').removeClass('active');
					$(this).addClass('active');					
				})
			});


			/*      PEGA VALOR DA LISTA      */

			function chamaJavaScript(componente){
				var dgValorLabel = componente.getAttribute("dg-data-valor");
				document.getElementById('dg-value-get-date').value = dgValorLabel;			
			};


			/*      PEGA VALUE TEXTAREA      */

			$('.jsTxtValue').keyup(function(){    
				var dgValorText = $('textarea[name=mensagem]').val()
				document.getElementById('dg-value-get-date').value = dgValorText;
			});			

			// SITE NOVO

			// SLIDER HOME BULLETS

			$('.md-bullets a[data-slide]').click(function(e) {
				$('.md-bullets a').removeClass('dg-ativo');
				$(this).addClass('dg-ativo');
		
				e.preventDefault();
				var slideno = $(this).data('slide');
				$('.md-slider-home').slick('slickGoTo', slideno - 1);
			});


			$('.navbar-toggler').click(function(){
                $('.collapse').addClass('jsMenu fadeInRight animated');
            })

            $('.md-slider-home').slick({
                dots: false,
                slidesToShow: 1,
                speed: 800,
                nextArrow : '<a href="javascript:void(0)" class="md-icon-next"> <i class="icon-arrow-right-a"></i> </a>',
                prevArrow : '<a href="javascript:void(0)" class="md-icon-prev"> <i class="icon-arrow-right-a"></i> </a>'

            });

            $('.md-slide-curtinas').slick({
                dots: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                speed: 500,
                infinite: false,
                arrows:false

			});


			$('.md-slider-prod').slick({
                dots: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                speed: 500,
                infinite: false,
                arrows:false

			});
			
			


            $('.jsAbriOrcamento').click(function(){

                $('.md-boxshadow').removeClass('fadeOutLeft animated');
                $('.solicitar-orcamento').removeClass('fadeOutRight animated');

                $('.md-boxshadow').show().addClass('fadeInLeft animated');
                $('main').addClass('md-blur');
                $('.solicitar-orcamento').show().addClass('fadeInRight animated');
                
            })

            $('.jsFechaOcamento').click(function(){
                $('.md-boxshadow').removeClass('fadeInLeft animated');
                $('.solicitar-orcamento').removeClass('fadeInRight animated')

                $('.md-boxshadow').addClass('fadeOutLeft animated');					
                $('main').removeClass('md-blur');
                $('.solicitar-orcamento').addClass('fadeOutRight animated');
                $('.md-boxshadow').fadeOut('slow');
                
            })

            $('.jsAbrirSearch').click(function(){
                $('.md-boxshadow').removeClass('fadeOut animated');
                $('.md-searc-p').removeClass('bounceOut animated');


                $('.md-boxshadow').fadeIn();
                $('main').addClass('md-blur');
                $('.md-searc-p').show().addClass('bounceIn animated');					

            })

            $('.md-boxshadow').click(function(){
                $('.md-boxshadow').removeClass('fadeIn animated');
                $('.md-boxshadow').removeClass('fadeInLeft animated');
                $('.md-searc-p').removeClass('bounceIn animated');
                $('.solicitar-orcamento').removeClass('fadeInRight animated');
                $('main').removeClass('md-blur');

                $('.solicitar-orcamento').addClass('fadeOutRight animated');
                $('.md-searc-p').addClass('bounceOut animated');
                $('.md-boxshadow').fadeOut('slow');
			})
			

			// MENU CORTINAS

			$('.jsOpenMenuCortinas').click(function(){
				$('.md-menu-cortinas').removeClass('fadeOutUp animated');
				$('.md-menu-cortinas').show().addClass('fadeInDown animated');
				$('main').addClass('md-blur');
			})
			
			$('.jsCLoseMenuCortinas').click(function(){
				$('.md-menu-cortinas').addClass('fadeOutUp animated');
				$('.md-menu-cortinas').removeClass('fadeInDown');
				$('main').removeClass('md-blur');
			})

			// $('main').click(function(){
			// 	$('.md-menu-cortinas').addClass('fadeOutUp animated');
			// 	$('.md-menu-cortinas').removeClass('fadeInDown');
			// 	$('main').removeClass('md-blur');
			// })

